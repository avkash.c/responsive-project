var map;
function initMap() {
  const loc = { lat: 23.044371, lng: 72.517921 };
  map = new google.maps.Map(document.getElementsByClassName("map")[0], {
    center: loc,
    zoom: 14
  });
  const marker = new google.maps.Marker({ position: loc, map: map });
  newMapLayout();
}

//newMapLayout();

function newMapLayout() {
  //alert("Hi");
  var mapimg = document.getElementsByClassName("map")[0];
  //mapimg.removeChild();
  var intrvl = setInterval(() => {
    //alert("inside");
    var imgs = mapimg.querySelectorAll("img[draggable][role]");

    imgs.forEach(e => {
      var src1 = e.src;
      src2 = src1.replace("2m3!1e2!6m1!3e5!3m17", "3m12");
      //   if (src1 == src2) {
      //     clearInterval(intrvl);
      //     break;
      //   }
      e.src = src2;
      //console.log(src1);
    });
  }, 100);
}
